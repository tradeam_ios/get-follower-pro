//
//  InAppPurchaseViewController.m
//  ParseStarterProject
//
//  Created by Kanwal on 4/23/14.
//
//

#import "InAppPurchaseViewController.h"
#import "DataHolder.h"
#import "InAppPurchaseManager.h"
#import "MBProgressHUD.h"
@interface InAppPurchaseViewController ()

@end

@implementation InAppPurchaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)PurchaseProduct:(id)sender{
    [MBProgressHUD showHUDAddedTo: self.view animated:YES];

    [[InAppPurchaseManager InAppPurchaseManagerSharedInstance] PurchaseProductWithNumber:[sender tag] Delegate:self WithSelector:@selector(Purchased:) WithErrorSelector:@selector(Error:)];
}
-(IBAction)Restore:(id)sender{

    [[InAppPurchaseManager InAppPurchaseManagerSharedInstance] Restore_ProductsWithDelegate:self WithSelector:@selector(Purchased:) WithErrorSelector:@selector(Error:)];
}
-(void)Purchased:(NSString*)product{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"ads"];
    if ([product isEqualToString:Coins_500]) {
        [self AddCoins:500];
    }
    if ([product isEqualToString:Coins_1000]) {
        [self AddCoins:1000];
    }
    if ([product isEqualToString:Coins_3000]) {
        [self AddCoins:3000];
    }
    if ([product isEqualToString:Coins_8000]) {
        [self AddCoins:8000];
    }
    if ([product isEqualToString:Coins_25000]) {
        [self AddCoins:25000];
    }
    if ([product isEqualToString:remove_ads]) {
        PFQuery *query = [PFQuery queryWithClassName:@"user"];
        [query getObjectInBackgroundWithId:[DataHolder DataHolderSharedInstance].UserObjectID block:^(PFObject *user, NSError *error) {
            // Do something with the returned PFObject in the gameScore variable.
            user[@"GOPRO"]=remove_ads;
            [user saveEventually:^(BOOL succeeded, NSError *error) {
                
                PFQuery *query = [PFQuery queryWithClassName:@"user"];
                [query getObjectInBackgroundWithId:[DataHolder DataHolderSharedInstance].UserObjectID block:^(PFObject *user, NSError *error) {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [DataHolder DataHolderSharedInstance].UserObject=user;
                    [[[UIAlertView alloc]initWithTitle:@"Pro Version!" message:@"Congratulations. Your app is now running on Pro Version. Enjoy!" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil]show];
                }];
            }];
        }];
        
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:remove_ads];
    }
    
    //updating label only.
}
-(void)AddCoins:(int)coins{
    [MBProgressHUD hideHUDForView:self.view animated:YES];

    [[DataHolder DataHolderSharedInstance].UserObject refresh];
    [DataHolder DataHolderSharedInstance].UserObject[@"coins"]=[NSNumber numberWithInt:[[DataHolder DataHolderSharedInstance].UserObject[@"coins"] intValue]+coins];
    [[DataHolder DataHolderSharedInstance].UserObject saveInBackground];

}
-(void)Error:(NSString*)error{

    if ([error isEqualToString:@"Transaction error"]) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    }else if(![error isEqualToString:@"Unlock_Functionality"]){
        [[AppManager AppManagerSharedInstance ] Show_Alert_With_Title:@"Error!" message:@"Unexpected Error Occured.Please Try Again Later"];
    }
}



-(IBAction)Back:(id)sender{

    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
