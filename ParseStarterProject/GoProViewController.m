//
//  GoProViewController.m
//  ParseStarterProject
//
//  Created by Kanwal on 5/24/14.
//
//

#import "GoProViewController.h"
#import "DataHolder.h"
#import "InAppPurchaseManager.h"
#import "MBProgressHUD.h"
@interface GoProViewController ()

@end

@implementation GoProViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    if ([[NSString stringWithFormat:@"%@",[DataHolder DataHolderSharedInstance].UserObject[@"GOPRO"]] isEqualToString:remove_ads]) {
        self.hide.hidden = YES;
    }else{
        self.hide.hidden = NO;

    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)PurchaseProduct:(id)sender{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[InAppPurchaseManager InAppPurchaseManagerSharedInstance] PurchaseProductWithNumber:6 Delegate:self WithSelector:@selector(Purchased:) WithErrorSelector:@selector(Error:)];
}
-(IBAction)Restore:(id)sender{
    
    [[InAppPurchaseManager InAppPurchaseManagerSharedInstance] Restore_ProductsWithDelegate:self WithSelector:@selector(Purchased:) WithErrorSelector:@selector(Error:)];
}
-(void)Purchased:(NSString*)product{
    
    if ([product isEqualToString:Coins_500]) {
        [self AddCoins:500];
    }
    if ([product isEqualToString:Coins_1000]) {
        [self AddCoins:1000];
    }
    if ([product isEqualToString:Coins_3000]) {
        [self AddCoins:3000];
    }
    if ([product isEqualToString:Coins_8000]) {
        [self AddCoins:8000];
    }
    if ([product isEqualToString:Coins_25000]) {
        [self AddCoins:25000];
    }
    if ([product isEqualToString:remove_ads]) {
        PFQuery *query = [PFQuery queryWithClassName:@"user"];
        [query getObjectInBackgroundWithId:[DataHolder DataHolderSharedInstance].UserObject.objectId block:^(PFObject *user, NSError *error) {
            // Do something with the returned PFObject in the gameScore variable.
            user[@"GOPRO"]=remove_ads;
            [user saveEventually:^(BOOL succeeded, NSError *error) {
                
                PFQuery *query = [PFQuery queryWithClassName:@"user"];
                [query getObjectInBackgroundWithId:[DataHolder DataHolderSharedInstance].UserObject.objectId block:^(PFObject *user, NSError *error) {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [DataHolder DataHolderSharedInstance].UserObject=user;
                    self.hide.hidden = YES;
                    [[[UIAlertView alloc]initWithTitle:@"Pro Version!" message:@"Congratulations. Your app is now running on Pro Version. Enjoy!" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:nil]show];
                    
                }];
                
            }];
            
        }];
        
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:remove_ads];
    }
    
    //updating label only.
}
-(void)AddCoins:(int)coins{
    
    PFQuery *query = [PFQuery queryWithClassName:@"user"];
    [query getObjectInBackgroundWithId:[DataHolder DataHolderSharedInstance].UserObjectID block:^(PFObject *user, NSError *error) {
        // Do something with the returned PFObject in the gameScore variable.
        int coin=[[user objectForKey:@"coins"] integerValue]+coins;
        user[@"coins"]=[NSNumber numberWithInt:coin];
        [user save];
    }];
    
}
-(void)Error:(NSString*)error{
    
    if ([error isEqualToString:@"Transaction error"]) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    }else if(![error isEqualToString:@"Unlock_Functionality"]){
        [[AppManager AppManagerSharedInstance ] Show_Alert_With_Title:@"Error!" message:@"Unexpected Error Occured.Please Try Again Later"];
    }
}

-(IBAction)Back:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
