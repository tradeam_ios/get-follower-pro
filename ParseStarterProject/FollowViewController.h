//
//  FollowViewController.h
//  ParseStarterProject
//
//  Created by Kanwal on 5/24/14.
//
//

#import <UIKit/UIKit.h>
#import "UserProfile.h"

@interface FollowViewController : UIViewController<UIActionSheetDelegate,UIAlertViewDelegate>{

    IBOutlet UIImageView *myimageView;
    IBOutlet UILabel *myName;
    IBOutlet UILabel *myNumberOfFollowers;
    IBOutlet UILabel *myNumberOfFollowing;
    IBOutlet UILabel *mytypeLabel;
    IBOutlet UILabel *mycoinsLabel;

    
    IBOutlet UIImageView *userimageView;
    IBOutlet UILabel *usersName;
    IBOutlet UILabel *userNumberOfFollowers;
    IBOutlet UILabel *userNumberOfFollowing;
    IBOutlet UILabel *userNumberOfPosts;
    IBOutlet UILabel *userDescription;
    IBOutlet UILabel *usertypeLabel;
    IBOutlet UIView *userroundView;
    PFObject *CurrentUserObject;
    UserProfile *CurrentUSerProfile;
    
    BOOL isShowingMenu;
    IBOutlet UIView *FollowView;
    UITapGestureRecognizer *HideTapGesture;
    
    IBOutlet UIButton *PromotionButton;
    IBOutlet UIButton *SkipButton;
    IBOutlet UIButton *LikeButton;

}
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *hudActivity;

@end
