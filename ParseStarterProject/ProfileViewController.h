//
//  ProfileViewController.h
//  ParseStarterProject
//
//  Created by Kanwal on 01/06/2014.
//
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController
@property(nonatomic,strong)NSString *username;
@property(nonatomic,strong)IBOutlet UIWebView *web;
@end
