//
//  FollowViewController.m
//  ParseStarterProject
//
//  Created by Kanwal on 5/24/14.
//
//

#import "FollowViewController.h"
#import "DataHolder.h"
#import "WebManager.h"
#import "JSONParser.h"
#import "UserProfile.h"
#import "UIImageView+WebCache.h"
#import "InAppPurchaseViewController.h"
#import "GoProViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "ProfileViewController.h"
#import "InAppPurchaseManager.h"


@interface FollowViewController (){
    
    BOOL firsTime;
}

    
@end

@implementation FollowViewController{

    UIDynamicAnimator* _animator;
    UIGravityBehavior* _gravity;
    UICollisionBehavior* _collision;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    firsTime = YES;

    mycoinsLabel.text=[NSString stringWithFormat:@"%@",[DataHolder DataHolderSharedInstance].UserObject[@"coins"]];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([UIScreen mainScreen].bounds.size.height==568) {
        FollowView.frame=CGRectMake(FollowView.frame.origin.x, FollowView.frame.origin.y, FollowView.frame.size.width, FollowView.frame.size.height+88);
    }
    
    isShowingMenu=NO;
    self.navigationController.navigationBarHidden=YES;
    myimageView.layer.cornerRadius=50;
    userimageView.layer.cornerRadius=50;
    userroundView.layer.cornerRadius=10;
    myimageView.layer.borderWidth=5.0;
    userimageView.layer.borderWidth=5.0;
    userroundView.layer.borderWidth=3.0;
    myimageView.layer.borderColor=[UIColor whiteColor].CGColor;
    userimageView.layer.borderColor=[UIColor grayColor].CGColor;
    userroundView.layer.borderColor=[UIColor grayColor].CGColor;
    [self ChangeSartStopButtonImageTiltle];
    
    //temp
    //return;
    
    mytypeLabel.text=[DataHolder DataHolderSharedInstance].UserObject[@"usertype"];
    usertypeLabel.text=@"All Topics";
    [DataHolder DataHolderSharedInstance].userSelectedType=@"All Topics";
    mycoinsLabel.text=[NSString stringWithFormat:@"%@",[DataHolder DataHolderSharedInstance].UserObject[@"coins"]];
    [self LoadMyData];
    [self LoadNewUser:nil];
    // Do any additional setup after loading the view from its nib.
}
-(void)LoadMyData{

    [[WebManager WebManagerSharedInstance] FetchUserObjectsWithUserID:@"self" Delegate:self WithSelector:@selector(UserDataParser:) WithErrorSelector:@selector(Error:)];
}
-(void)UserDataParser:(NSData*)data{

    [DataHolder DataHolderSharedInstance].UserProfile=[[JSONParser JSONParserSharedInstance] ParseUserProfileObjects:data];
    [self LoadMyDataLabels];
}
-(void)LoadMyDataLabels{

    myName.text=[DataHolder DataHolderSharedInstance].UserProfile.username;
    myNumberOfFollowers.text=[DataHolder DataHolderSharedInstance].UserProfile.followed_by;
    myNumberOfFollowing.text=[DataHolder DataHolderSharedInstance].UserProfile.follows;
    [myimageView setImageWithURL:[NSURL URLWithString:[DataHolder DataHolderSharedInstance].UserProfile.profile_picture]];
    mytypeLabel.text=[DataHolder DataHolderSharedInstance].UserObject[@"usertype"];
    usertypeLabel.text=@"All Topics";
    [DataHolder DataHolderSharedInstance].userSelectedType=@"All Topics";
    mycoinsLabel.text=[NSString stringWithFormat:@"%@",[DataHolder DataHolderSharedInstance].UserObject[@"coins"]];
    if (![[DataHolder DataHolderSharedInstance].UserObject[@"username"] isEqualToString:[DataHolder DataHolderSharedInstance].UserProfile.username]) {
        [DataHolder DataHolderSharedInstance].UserObject[@"username"]=[DataHolder DataHolderSharedInstance].UserProfile.username;
        [[DataHolder DataHolderSharedInstance].UserObject saveInBackground];
    }
    
    
}
-(void)loadUserLabels{

    usersName.text=CurrentUSerProfile.username;
    userNumberOfFollowers.text=CurrentUSerProfile.followed_by;
    userNumberOfFollowing.text=CurrentUSerProfile.follows;
    userDescription.text=CurrentUSerProfile.bio;
    userNumberOfPosts.text=CurrentUSerProfile.media;
    [userimageView setImageWithURL:[NSURL URLWithString:CurrentUSerProfile.profile_picture]];
    [self SetEnableUserInteraction];
    
}
-(IBAction)LoadNewUser:(id)sender{

    [self SetDisableUserInteraction];
    PFQuery *query=[PFQuery queryWithClassName:@"user"];
    [query whereKey:@"isOnPromotion" equalTo:[NSNumber numberWithBool:YES]];
    [query whereKey:@"appfollows" notEqualTo:[DataHolder DataHolderSharedInstance].UserObject[@"userId"]];
    if (![[DataHolder DataHolderSharedInstance].userSelectedType isEqualToString:@"All Topics"]) {
        [query whereKey:@"usertype" equalTo:[DataHolder DataHolderSharedInstance].userSelectedType];
    }
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %d scores.", objects.count);
            // Do something with the found objects
            if (objects.count!=0) {
       
                [[DataHolder DataHolderSharedInstance].usersArray removeAllObjects];
                [[DataHolder DataHolderSharedInstance].usersArray addObjectsFromArray:objects];
                [self CheckUserFromInstagramAndShow];
                
            }
            else{
                
                if (firsTime == YES) {
                    self.hudActivity.hidden = YES;
                    firsTime = NO;
                    [[[UIAlertView alloc]initWithTitle:@"There is no more user to follow." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
                }
            
                [self LoadNewUser:nil];
                
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
            [self LoadNewUser:nil];
        }
    }];
    

    
    
}
-(void)CheckUserFromInstagramAndShow{

    [self SetDisableUserInteraction];
    if ([DataHolder DataHolderSharedInstance].usersArray.count!=0) {
        CurrentUserObject=[[DataHolder DataHolderSharedInstance].usersArray objectAtIndex:0];
        [CurrentUserObject refreshInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (CurrentUserObject[@"isOnPromotion"]) {
                [[WebManager WebManagerSharedInstance] GetRelationWithUserWithID:CurrentUserObject[@"userId"] Delegate:self WithSelector:@selector(RelationCallBack:) WithErrorSelector:@selector(Error:)];
            }
            else{
            
                [[DataHolder DataHolderSharedInstance].usersArray removeObject:CurrentUserObject];
                [self CheckUserFromInstagramAndShow];
            }
        }];
    }
    else{
        
        if (firsTime == YES) {
            self.hudActivity.hidden = YES;
            firsTime = NO;
            [[[UIAlertView alloc]initWithTitle:@"There is no more user to follow." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
        
    
        [self LoadNewUser:nil];
    }
    
}
-(void)RelationCallBack:(NSData*)data{

    if ([[JSONParser JSONParserSharedInstance] ParseRelationObject:data]) {
        [[WebManager WebManagerSharedInstance] FetchUserObjectsWithUserID:CurrentUserObject[@"userId"] Delegate:self WithSelector:@selector(UserDataCallBack:) WithErrorSelector:@selector(Error:)];
    }
    else{
        if (firsTime == YES) {
            self.hudActivity.hidden = YES;
            firsTime = NO;
            [[[UIAlertView alloc]initWithTitle:@"There is no more user to follow." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        }
        
        [[DataHolder DataHolderSharedInstance].usersArray removeObject:CurrentUserObject];
        [self CheckUserFromInstagramAndShow];
        
    }
    
}
-(void)UserDataCallBack:(NSData*)data{
    
    self.hudActivity.hidden = NO;
    firsTime = YES;
    

    CurrentUSerProfile=[[JSONParser JSONParserSharedInstance] ParseUserProfileObjects:data];
    [self loadUserLabels];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)Follow:(id)sender{

    [[WebManager WebManagerSharedInstance] FollowUserWithID:CurrentUserObject[@"userId"] Delegate:self WithSelector:@selector(FollowCallBack:) WithErrorSelector:@selector(Error:)];
}
-(void)FollowCallBack:(NSData*)data{
    [self SetDisableUserInteraction];
    if([[JSONParser JSONParserSharedInstance] ParseFollowResponseObject:data]){
    
        int coins=[CurrentUserObject[@"coins"] integerValue];
        coins-=3;
        if (coins<3) {
            CurrentUserObject[@"isOnPromotion"]=[NSNumber numberWithBool:NO];
        }
        CurrentUserObject[@"coins"]=[NSNumber numberWithInt:coins];
        NSMutableArray *arr=[[NSMutableArray alloc] initWithArray:CurrentUserObject[@"appfollows"]];
        [arr addObject:[DataHolder DataHolderSharedInstance].UserID];
        CurrentUserObject[@"appfollows"]=arr;
        [CurrentUserObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [[DataHolder DataHolderSharedInstance].usersArray removeObject:CurrentUserObject];
            [self CheckUserFromInstagramAndShow];
        }];
        int mycoins=[[DataHolder DataHolderSharedInstance].UserObject[@"coins"] integerValue];
        
        if ([[NSString stringWithFormat:@"%@",[DataHolder DataHolderSharedInstance].UserObject[@"GOPRO"]] isEqualToString:remove_ads]) {
            [DataHolder DataHolderSharedInstance].UserObject[@"coins"]=[NSNumber numberWithInt:mycoins+2];
        }else{
            [DataHolder DataHolderSharedInstance].UserObject[@"coins"]=[NSNumber numberWithInt:mycoins+1];

        }

        
        [[DataHolder DataHolderSharedInstance].UserObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [self LoadMyDataLabels];
        }];
    }
    else{
    
        [[AppManager AppManagerSharedInstance] Show_Alert_With_Title:@"Error" message:@"You can not follow the user at this time. this error is from Instagram.\nPlease try again after some time"];
    }
}

-(IBAction)Skip:(id)sender{

    [[DataHolder DataHolderSharedInstance].usersArray removeObject:CurrentUserObject];
    [self CheckUserFromInstagramAndShow];
    
}

-(IBAction)ProTapped:(id)sender{

    GoProViewController *Obj=[[GoProViewController alloc] initWithNibName:@"GoProViewController" bundle:nil];
    [self presentViewController:Obj animated:YES completion:nil];
}
-(IBAction)GetTokenTapped:(id)sender{

    InAppPurchaseViewController *Obj=[[InAppPurchaseViewController alloc] initWithNibName:@"InAppPurchaseViewController" bundle:nil];
    [self presentViewController:Obj animated:YES completion:nil];
}
-(IBAction)StartStopPromotion:(id)sender{

    [[DataHolder DataHolderSharedInstance].UserObject refresh];
    if ([[DataHolder DataHolderSharedInstance].UserObject[@"isOnPromotion"] boolValue]) {
        [DataHolder DataHolderSharedInstance].UserObject[@"isOnPromotion"]=[NSNumber numberWithBool:NO];
        [[DataHolder DataHolderSharedInstance].UserObject saveInBackground];
    }
    else{
        if ([[DataHolder DataHolderSharedInstance].UserObject[@"coins"] integerValue]>=10) {
            [DataHolder DataHolderSharedInstance].UserObject[@"isOnPromotion"]=[NSNumber numberWithBool:YES];
            [[DataHolder DataHolderSharedInstance].UserObject saveInBackground];
        }
        else{
        
            [[AppManager AppManagerSharedInstance] Show_Alert_With_Title:@"Sorry!" message:@"you can start promotion with atleast 10 tokens"];
        }
        
    }
    //[self ChangeSartStopButtonImageTiltle];
}
-(void)ChangeSartStopButtonImageTiltle{

    if ([[DataHolder DataHolderSharedInstance].UserObject[@"isOnPromotion"] boolValue]) {
    
        [PromotionButton setBackgroundImage:[UIImage imageNamed:@"logout.png.png"] forState:UIControlStateNormal];
        [PromotionButton setTitle:@"Stop" forState:UIControlStateNormal];
        
    }
    else{
    
        [PromotionButton setBackgroundImage:[UIImage imageNamed:@"promote.png"] forState:UIControlStateNormal];
        [PromotionButton setTitle:@"Start Promotion" forState:UIControlStateNormal];
    }
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationCurveLinear
                     animations:^{
                         PromotionButton.transform=CGAffineTransformMakeScale(1.1, 1.1);
                     }
                     completion:^(BOOL finished){
                         
                         [UIView animateWithDuration:0.3
                                               delay:0.0
                                             options: UIViewAnimationCurveLinear
                                          animations:^{
                                              PromotionButton.transform=CGAffineTransformMakeScale(1.0, 1.0);
                                          }
                                          completion:^(BOOL finished){
                                              
                                              [self performSelector:@selector(ChangeSartStopButtonImageTiltle) withObject:nil afterDelay:0.2];
                                          }];
                     }];
}
-(IBAction)LogoutTapped:(id)sender{

    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Logout" message:@"Are you sure want to logout?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [alert show];
}
-(IBAction)SetCategoryTapped:(id)sender{

    UIActionSheet *actionsheet=[[UIActionSheet alloc] initWithTitle:@"Select your category" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"All Topics", @"Abstract",@"Advertising",@"Architecture",@"Art",@"Cars",@"Cities",@"Entertainment",@"Fashion",@"Food",@"Health & Fittness",@"Nature",@"Personal Life",@"Sports",@"Travel", nil];
    actionsheet.tag=1;
    [actionsheet showInView:self.view];
}
-(IBAction)SetMyCategory:(id)sender{

    UIActionSheet *actionsheet=[[UIActionSheet alloc] initWithTitle:@"Select your category" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Abstract",@"Advertising",@"Architecture",@"Art",@"Cars",@"Cities",@"Entertainment",@"Fashion",@"Food",@"Health & Fittness",@"Nature",@"Personal Life",@"Sports",@"Travel", nil];
    actionsheet.tag=0;
    [actionsheet showInView:self.view];
}
-(IBAction)ShowMenu:(id)sender{
    
    if (isShowingMenu) {
        [self HideMenu:nil];
        return;
    }
    
    
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationCurveLinear
                     animations:^{
                         FollowView.frame=CGRectMake(0, self.view.frame.size.height-100, FollowView.frame.size.width, FollowView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         
                         HideTapGesture=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(HideMenu:)];
                         [self.view addGestureRecognizer:HideTapGesture];
                         isShowingMenu=YES;
                     }];
    
}
-(IBAction)HideMenu:(id)sender{
    
    [self.view removeGestureRecognizer:HideTapGesture];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         FollowView.frame=CGRectMake(0, 50, FollowView.frame.size.width, FollowView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         isShowingMenu=NO;
                     }];
    
}
-(void)Error:(NSError*)error{
    
    [[AppManager AppManagerSharedInstance] Show_Alert_With_Title:@"Error" message:@"Pease try again later"];
}
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex==1) {
        [[NSURLCache sharedURLCache] removeAllCachedResponses];
        for(NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) {
            [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
        }
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}
-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{

    NSLog(@"%d",buttonIndex);
    if (actionSheet.tag==0) {
        if (buttonIndex!=14) {
            [DataHolder DataHolderSharedInstance].UserObject[@"usertype"]=[[DataHolder DataHolderSharedInstance].userTypes objectAtIndex:buttonIndex];
            [[DataHolder DataHolderSharedInstance].UserObject saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                mytypeLabel.text=[DataHolder DataHolderSharedInstance].UserObject[@"usertype"];
            }];
        }
    }
    else if (actionSheet.tag==1) {
        if (buttonIndex!=15) {
            if (buttonIndex==0) {
                [DataHolder DataHolderSharedInstance].userSelectedType=@"All Topics";
            }
            else{
            
                [DataHolder DataHolderSharedInstance].userSelectedType=[[DataHolder DataHolderSharedInstance].userTypes objectAtIndex:buttonIndex-1];
            }
            usertypeLabel.text=[DataHolder DataHolderSharedInstance].userSelectedType;
            [self LoadNewUser:nil];
        }
        
    }
}
-(void)SetDisableUserInteraction{

    [userroundView setHidden:YES];
    LikeButton.enabled=NO;
    SkipButton.enabled=NO;
}
-(void)SetEnableUserInteraction{

    [userroundView setHidden:NO];
    LikeButton.enabled=YES;
    SkipButton.enabled=YES;
}
-(IBAction)GotouserProfile{

    ProfileViewController *OBj=[[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
    OBj.username=CurrentUserObject[@"username"];
    [self.navigationController pushViewController:OBj animated:YES];
    
}
@end
