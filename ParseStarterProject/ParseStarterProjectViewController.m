#import "ParseStarterProjectViewController.h"
#import <Parse/Parse.h>
#import "IGLoginViewController.h"

@implementation ParseStarterProjectViewController


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - UIViewController

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    loginBtn.layer.cornerRadius=10;
    self.navigationController.navigationBarHidden = YES;
}

-(IBAction)LoginIG:(id)sender{
    
    IGLoginViewController *Obj=[[IGLoginViewController alloc] initWithNibName:@"IGLoginViewController" bundle:nil];
    [self.navigationController pushViewController:Obj animated:YES];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end



